from django.contrib import admin
from realtime_chatapp.models import Team, Message, Friends, Profile


admin.site.register(Team)
admin.site.register(Message)
admin.site.register(Friends)
admin.site.register(Profile)

# Register your models here.
