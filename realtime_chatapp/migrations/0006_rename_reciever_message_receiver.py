# Generated by Django 4.1.3 on 2022-11-23 07:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realtime_chatapp', '0005_alter_friends_options_remove_message_team_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='message',
            old_name='reciever',
            new_name='receiver',
        ),
    ]
