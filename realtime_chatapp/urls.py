from django.urls import path
from realtime_chatapp import views


urlpatterns = [
    path(
        "accounts/login/",
        views.Login.as_view(),
        name="login",
    ),
    path(
        "",
        views.Home.as_view(),
        name="home",
    ),
    path(
        "chat_room/<int:pk>/",
        views.Chat.as_view(),
        name="chat_room",
    ),
    path(
        "accounts/register/",
        views.RegisterView.as_view(),
        name="register",
    ),
    path(
        "profile/<int:pk>/",
        views.ProfileView.as_view(),
        name="profile",
    ),
    path(
        "update_profile/",
        views.UpdateProfileView.as_view(),
        name="update_profile",
    ),
    path(
        "register/next/<int:pk>/",
        views.RegisterNext.as_view(),
        name="register_next",
    ),
    path(
        "create_profile/", 
        views.CreateProfileView.as_view(), 
        name="create_profile",
    ),
    path(
        "friend/profile/<int:pk>/", 
        views.FriendsProfile.as_view(), 
        name="friend_profile",
    ),
]
