from django.views.generic import View, CreateView, DetailView, UpdateView
from django.shortcuts import redirect, render
from django.contrib.auth.models import User
from realtime_chatapp.forms import RegisterForm, UploadProfileForm
from django.contrib.auth import login
from django.contrib.auth.views import LoginView
from django.urls import reverse
from realtime_chatapp.models import Profile, Message
from django.http import HttpResponse
import json


class Login(LoginView):
    def get_success_url(self):
        print("Logged in user :", self.request.user)
        profile = Profile.objects.filter(user__id = self.request.user.id).exists()
        if profile:
            return reverse("profile", kwargs={"pk": self.request.user.pk})
        else:
            return reverse("home")


class Home(View):
    def get(self, request):
        context = {}
        profile = Profile.objects.filter(user__id = self.request.user.id).exists()
        context["profile"] = profile

        return render(request, "registration/home.html", context)


class RegisterView(CreateView):
    model = User
    template_name = "registration/register.html"
    form_class = RegisterForm

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        profile_user = Profile.objects.get(user=self.request.user)
        return redirect("register_next", pk=profile_user.id)


class ProfileView(DetailView):
    model = User
    template_name = "registration/profile.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProfileView, self).get_context_data(*args, **kwargs)
        try:
            profile_user = Profile.objects.get(user__id = self.kwargs["pk"])
        except Exception as e:
            return context
        
        context["profile_user"] = profile_user

        context["all_users"]  = Profile.objects.all().exclude(id=profile_user.id)

        return context


class UpdateProfileView(View):
    def get(self,request):
        return render(request,"registration/update_profile.html")

    def post(self,request):
        return redirect("home")


class CreateProfileView(CreateView):
    model = Profile
    template_name = "registration/profile_upload.html"
    form_class = UploadProfileForm

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.save()
        return redirect("profile", pk=self.request.user.id)


class RegisterNext(UpdateView):
    model = Profile
    template_name = "registration/profile_upload.html"
    form_class = UploadProfileForm


class FriendsProfile(DetailView):
    model = User
    template_name = "registration/friends_profile.html"

    def get_context_data(self, *args, **kwargs):
        context = super(FriendsProfile, self).get_context_data(*args, **kwargs)
        try:
            profile_user = Profile.objects.get(user__id = self.kwargs["pk"])
        except Exception as e:
            return context
        
        context["profile_user"] = profile_user

        return context


class Chat(View):

    def get(self, request,pk):
        context = {}
        receiver = User.objects.get(id=pk)
        messages = Message.objects.filter(sender=self.request.user, receiver=receiver)
        context["messages"] = messages
        chat_user = Profile.objects.get(user__id = pk)
        context["chat_user"] = chat_user
        return render(request, "registration/chat_room.html", context)


    def post(self, request, pk):
        message = request.POST.get("message")
        receiver = request.POST.get("receiver_id")
        
        try:
            if receiver:
                receiver_user = User.objects.get(id=receiver)
            
            else:
                raise ValueError("Login to send message..")
            
            
            sender_user = self.request.user
            
            Message.objects.create(
                value = message,
                sender = sender_user,
                receiver = receiver_user,
            )

        except Exception as e:
            pass

        return HttpResponse(
            "Message Sent successfully.."
        )

