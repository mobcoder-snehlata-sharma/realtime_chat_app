from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from realtime_chatapp.models import Profile


class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            "username",
            "password1",
            "password2",
        ]

    def save(self):
        user = super().save()
        profile = Profile(user=user)
        profile.save()
        return user


class UpdateUser(UserChangeForm):
    class Meta:
        model = User
        fields = "__all__"


class UploadProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = "__all__"
