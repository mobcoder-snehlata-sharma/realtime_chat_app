from django.apps import AppConfig


class RealtimeChatappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'realtime_chatapp'
