from django.db import models
from datetime import datetime
from django.contrib.auth.models import User


class Profile(models.Model):
    date_of_birth = models.DateField(
        default="1999-01-01",
    )
    profile_avatar = models.ImageField(
        blank=True,
        null=True,
        upload_to="images/avatar",
    )
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.user.username}'s Profile"


class Team(models.Model):
    name = models.CharField(
        max_length=1000,
    )


class Message(models.Model):
    value = models.CharField(
        max_length=100000,
    )
    date = models.DateTimeField(
        default=datetime.now,
        blank=True,
        null=True,
    )
    sender = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="sender_user",
    )
    receiver = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="reciever_user",
    )


class Friends(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name="login_user",
    )
    friends = models.ManyToManyField(
        User,
        related_name="friends",
        blank=True,
    )

    class Meta:
        verbose_name = "Friends"
        verbose_name_plural = "Friends"
