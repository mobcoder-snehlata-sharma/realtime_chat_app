function message_send(){
    
    var receiver_id = $(this).data("receiver");
    var message = $("#message").val();
    var sender_id = $(this).data("sender");
    postData = {
        "message" : message,
        "sender_id" : sender_id,
        "receiver_id" : receiver_id,
        "csrfmiddlewaretoken" : $('input[name=csrfmiddlewaretoken]').val(),
    }
    
    $.ajax({
        type : "POST",
        url : "/chat_room/" + receiver_id+"/",
        data : postData,
        success : function(data){
            alert(data);
        }
    });
    $("#message").val("")
}

$(document).ready(function(){

    $(document).on("click", "#message_send_btn", message_send);
});